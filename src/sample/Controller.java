package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;

public class Controller {

    private static final String MUSIC = "\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")";
    private static final String IMG = "(?<=href=\")[^>]*/*(file)(?=\"(\\s+|>))";
    @FXML
    private AnchorPane playerMenu;

    @FXML
    private Button pause;

    @FXML
    private Button on;

    @FXML
    private Button nextButton;

    @FXML
    private Button play;

    @FXML
    private AnchorPane imgMenu;

    @FXML
    private ToggleGroup imgGroup;

    @FXML
    private TextField pictureLinc;

    @FXML
    private TextField picturePath;

    @FXML
    private AnchorPane musicMenu;

    @FXML
    private ToggleGroup musicGroup;

    @FXML
    private TextField musicLinc;

    @FXML
    private TextField musicPath;

    @FXML
    private CheckBox musicButton;

    @FXML
    private CheckBox imgButton;
    private MusicPlayer musicPlayer;
    /**
     * Метод скачивает музыку и картинку
     *
     * @param event нажатие на кнопку "Скачать"
     */
    @FXML
    public void download(ActionEvent event) {
        try {
            if (musicButton.isSelected()) {
                Downloader downloader = new Downloader(new Searcher(musicLinc.getText(), MUSIC).start(), musicPath.getText(), getAmount(musicGroup), "music", "mp3");
                downloader.start();
                downloader.join();
                playerMenu.setVisible(true);
            }
            if (imgButton.isSelected()) {
                Downloader downloader = new Downloader(new Searcher(pictureLinc.getText(), IMG).start(), picturePath.getText(), getAmount(imgGroup), "img", "jpg");
                downloader.start();
                downloader.join();
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Скачивание завершено");
    }

    /**
     * Метод воспроизводит музыку
     *
     * @param event нажатие на кнопку "Воспроизвести"
     */
    @FXML
    public void musicOn(ActionEvent event) {
        musicPlayer = new MusicPlayer(musicPath.getText());
        musicPlayer.play();
        on.setVisible(false);
        pause.setVisible(true);
        play.setVisible(true);
        nextButton.setVisible(true);

    }

    /**
     * Ставит трек на паузу
     *
     * @param event нажатие на кнопку "Pause"
     */
    public void pauseMusic(ActionEvent event) {
        musicPlayer.pause();
        pause.setVisible(false);
    }

    /**
     * Метод запускает следующй трек
     *
     * @param event нажатие на кнопку "Next"
     */
    public void next(ActionEvent event) {
      musicPlayer.next();
    }

    private int getAmount(ToggleGroup buttonGroup) {
        ToggleButton button = (ToggleButton) buttonGroup.getSelectedToggle();
        return Integer.parseInt(button.getText());
    }

    /**
     * Делает меню музыкивидимым
     */
    public void musicVisible(ActionEvent event) {
        musicMenu.setVisible(!musicMenu.isVisible());
    }

    /**
     * Делает меню картинок видимым
     */
    public void imgVisible(ActionEvent event) {
        imgMenu.setVisible(!imgMenu.isVisible());
    }

    public void playMusic(ActionEvent event) {
        musicPlayer.play();
        pause.setVisible(true);
    }

    public void musicOff(ActionEvent event) {
        musicPlayer.stop();
        on.setVisible(true);
        pause.setVisible(false);
        play.setVisible(false);
        nextButton.setVisible(false);

    }

    public void close(MouseEvent mouseEvent) {
        System.exit(0);
    }
}
