package sample;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class MusicPlayer {
    private File file;
    private int musicNumber;
    private String pathToMusic;
    private MediaPlayer mediaPlayer;

    public MusicPlayer(String pathToMusic) {
        this.pathToMusic = pathToMusic;
        musicNumber = 0;
        file = getFile(pathToMusic);
        mediaPlayer = new MediaPlayer(new Media(file.toURI().toString()));
    }

    private File getFile(String pathToMusic) {
        return new File(pathToMusic + "\\" + "music" + musicNumber + ".mp3");
    }

    void pause() {
        mediaPlayer.pause();
    }

    void play() {
        mediaPlayer.play();
    }

    void stop() {
        mediaPlayer.stop();
    }

    void next() {
        musicNumber++;
        file = getFile(pathToMusic);
        if (!file.exists()) {
            musicNumber = 0;
            file = getFile(pathToMusic);
        }
        mediaPlayer.stop();
        mediaPlayer = new MediaPlayer(new Media(file.toURI().toString()));
        mediaPlayer.play();
    }
}
