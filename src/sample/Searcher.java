/**
 * Класс предназначен для поиска ссылки на скачивание на сайте
 *
 * @author Андрианова М.А
 */

package sample;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class Searcher {
    private String input;
    private String regex;

    Searcher(String input, String regex) {
        this.input = input;
        this.regex = regex;
    }

    List<String> start() throws IOException {
        List<String> output = new ArrayList<>();
        URL url = new URL(input);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String result = bufferedReader.lines().collect(Collectors.joining("\n"));
            Pattern email_pattern = Pattern.compile(regex);
            Matcher matcher = email_pattern.matcher(result);
            while(matcher.find()) {
                output.add(matcher.group());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return output;
    }
}