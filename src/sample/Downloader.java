package sample;
/**
 * Класс предназначен для скачивания музыки и картинок
 */

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

public class Downloader extends Thread {
    private List<String> inputPath;
    private String outputPath;
    private int amount;
    private String name;
    private String type;

    Downloader(List<String> inputPath, String outputPath, int amount, String name, String type) {
        this.inputPath = inputPath;
        this.outputPath = outputPath;
        this.amount = amount;
        this.name = name;
        this.type = type;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < amount; i++) {
                URL url = new URL(inputPath.get(i));
                InputStream inputStream = url.openStream();
                Files.copy(inputStream, Paths.get(outputPath + "\\" + name + i + "." + type), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
